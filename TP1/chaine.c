/*chaine.c*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h"
#include <assert.h>

void init_chaine (struct chaine *C){
	C->s=(char *)0;
	C->alloc=0;
	C->size=0;
}

void clear_chaine (struct chaine *C){
	free(C->s);
}

void ajout_caractere (struct chaine *C,char a){
	if(C->size+1>=C->alloc){
		C->s=realloc(C->s,(C->size+8)*sizeof(char));
		assert(C->s !=(char*)0);
		C->alloc =C->size+8;
	}
	C->s[C->size]=a;
	C->size+=1;
	C->s[C->size]='\0';
}

void afficher_chaine(struct chaine *C){
	if(C->size==0){
		printf("\n");
	}
	else{
		printf("%s\n",C->s);
	}
}
