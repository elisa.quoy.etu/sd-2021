/*chaine.h*/

struct chaine {
	char *s;
	int alloc;
	int size;
};		

extern void init_chaine(struct chaine*);
extern void clear_chaine(struct chaine*);
extern void ajout_caractere(struct chaine*,char);
extern void afficher_chaine(struct chaine*);


