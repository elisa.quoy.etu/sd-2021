/*vim liste_char.h*/

struct chaine{
	char c;
	struct chaine* suivant;
};

#define NIL (struct chaine*)0

struct liste{
	struct chaine* tete;
	int nbelem;
};

extern void init_liste(struct liste*);
extern void clear_liste(struct liste*);
extern void ajout_en_tete(struct liste*,char);
extern void afficher_liste(struct liste);
