/*main.c*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include "liste_char.h"

int main(){
	struct liste L;
	int c;
	init_liste(&L);
	c=getchar();
	while (! isspace (c)){
		ajout_en_tete(&L,(char)c);
		c=getchar();
	}
	afficher_liste(L);
	clear_liste(&L);
	return 0;
}
