#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main(){
	int i;
	int c;
	char* s;
	s=(char *) malloc (64*sizeof(char));
	i=0;
	c=getchar();
	while(! isspace (c)){
		s[i]=c;
		i=i+1;
		c=getchar();
	}
	s[i]='\0';
	printf("%s\n",s);
	free(s);
	return 0;
}
