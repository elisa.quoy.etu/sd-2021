/*main.c*/

#include "chaine.h"
#include <stdio.h>
#include <ctype.h>

int main(){
	struct chaine s;
	int c;
	init_chaine(&s);
	c=getchar();
	while (! isspace (c)){
		ajout_caractere(&s,(char)c);
		c=getchar();
	}
	afficher_chaine(&s);
	clear_chaine(&s);
	return 0 ;
}
