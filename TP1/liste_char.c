/*liste_char.c*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "liste_char.h"

void init_liste (struct liste* L){
	L->tete=NIL;
	L->nbelem=0;
}

void clear_liste (struct liste* L){
	struct chaine* C;
	struct chaine* suiv;
	C = L->tete;
	for (int i=0; i<L->nbelem; i++){
		suiv = C->suivant;
		free(C);
		C=suiv;
	}
}

void ajout_en_tete (struct liste* L, char s){
	struct chaine* C;
	C= (struct chaine*) malloc (sizeof(struct chaine));
	C->c = s;
	C->suivant = L->tete;
	L->tete = C;
	L->nbelem += 1;
}

void afficher_liste (struct liste L){
	struct chaine* C;
	C = L.tete;
	for (int i=0; i<L.nbelem; i++){
		printf("%s", C->c);
		C = C->suivant;
	}
	printf ("\n");
}
